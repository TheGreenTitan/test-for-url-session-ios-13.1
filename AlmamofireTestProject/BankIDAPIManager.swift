//
//  BankIDAPIManager.swift
//  APIManager
//
//  Created by Menjie Mao on 2019-07-02.
//  Copyright © 2019 Menjie Mao. All rights reserved.


import Alamofire
import SwiftyJSON

private enum RequestJsonTag: String {
    case autoStartToken                     = "autoStartToken"
    case orderRef                           = "orderRef"
    case signature                          = "signature"
    case userInfo                           = "userInfo"
    case ocspResponse                       = "ocspResponse"
    case status                             = "status"
    case hintCode                           = "hintCode"
    case completionData                     = "completionData"
}

private enum ResponseJsonTag: String{
    case result                        = "result"
    case errorMessage                  = "errorMessage"
}

typealias ModelCompletion<T> = (ModelResult<T>) -> Void

/// Model Result
enum ModelResult<T> {
    case success(T)
    case failure(error: String)
    
    /// Returns `true` if the result is a success, `false` otherwise.
    public var isSuccess: Bool {
        switch self {
        case .success:
            return true
        case .failure:
            return false
        }
    }
}

class BankIDManager: NSObject, URLSessionDelegate {
    
    var autoStartToken: String?
    var orderRef: String?
    var config: URLSessionConfiguration?
    var session: URLSession?
    
    // Shared instance
    static let sharedInstance: BankIDManager = {
        let instance = BankIDManager()
        return instance
    }()
    
    override init() {
        super.init()
    }
    
    func authenticate(completion: @escaping ModelCompletion<Bool>) {
        let url = String(format: "https://appapi2.bankid.com/rp/v5/auth")
        guard let serviceUrl = URL(string: url) else {
            return
        }
        
        let endUserIp: String = self.getIPAddress() ?? "192.168.8.103"
        let parameters: Parameters = ["endUserIp": endUserIp]
        
        var request = URLRequest(url: serviceUrl)
        request.httpMethod = "POST"
        request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) else {
            return
        }
        request.httpBody = httpBody
        
        config = URLSessionConfiguration.default
        session = URLSession(configuration: config!, delegate: self, delegateQueue: OperationQueue.current)
        session?.dataTask(with: request) { [weak self] (data, response, error) in
            print(response)
        }.resume()
    }
    
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        let protectionSpace = challenge.protectionSpace

        switch (protectionSpace.authenticationMethod, protectionSpace.host) {
            
        case (NSURLAuthenticationMethodClientCertificate, "appapi2.bankid.com"):
            let identity = Bundle.main.identity(named: "bankidfinal", password: "qwerty123")
            let intermediate1 = Bundle.main.certificate(named: "bankidpdf")
            let credential = URLCredential(identity: identity, certificates: [intermediate1], persistence: .forSession)
            
            completionHandler(.useCredential, credential)
            
        default:
            completionHandler(.performDefaultHandling, nil)
        }
    }
    
//    private func didReceiveSessionManagerChallenge(delegate: SessionDelegate) {
//
//        // Did Receive Challenge
//        delegate.sessionDidReceiveChallenge = { urlSession, challenge in
//
//            let protectionSpace = challenge.protectionSpace
//            NSLog("task challenge %@", protectionSpace.authenticationMethod)
//
//            switch (protectionSpace.authenticationMethod, protectionSpace.host) {
//            case (NSURLAuthenticationMethodServerTrust, "appapi2.test.bankid.com"):
//                if self.shouldTrustBankID(protectionSpace: protectionSpace) {
//                    let credential = URLCredential(trust: protectionSpace.serverTrust!)
//                    return (.useCredential, credential)
//                } else {
//                    return (.cancelAuthenticationChallenge, nil)
//                }
//            case (NSURLAuthenticationMethodClientCertificate, "appapi2.test.bankid.com"):
//                let identity = Bundle.main.identityForBankIDPayment(named: "bankidfinal", password: "qwerty123")
//                let credential = URLCredential(identity: identity, certificates: nil, persistence: .forSession)
//                return (.useCredential, credential)
//
//            default:
//                return (.performDefaultHandling, nil)
//            }
//        }
//    }
//
//    private func shouldTrustBankID(protectionSpace: URLProtectionSpace) -> Bool {
//
//        guard let trust = protectionSpace.serverTrust else { return false }
//
//        // First try evaluating trust with any custom anchor.  If that succeeds, we're good to go.
//
//        var trustResult = SecTrustResultType.invalid
//        var err = SecTrustEvaluate(trust, &trustResult)
//        guard err == errSecSuccess else { return false }
//        if [.proceed, .unspecified].contains(trustResult) { return true }
//
//        // If it fails, apply our custom anchor and try again.
//
//        let root = Bundle.main.certificateForBankIDPayment(named: "bankidpdf")
//        err = SecTrustSetAnchorCertificates(trust, [root] as NSArray)
//        guard err == errSecSuccess else { return false }
//        err = SecTrustEvaluate(trust, &trustResult)
//        guard err == errSecSuccess else { return false }
//        return [.proceed, .unspecified].contains(trustResult)
//    }
    
    private func getIPAddress() -> String? {
        var address: String?
        var ifaddr: UnsafeMutablePointer<ifaddrs>? = nil
        if getifaddrs(&ifaddr) == 0 {
            var ptr = ifaddr
            while ptr != nil {
                defer { ptr = ptr?.pointee.ifa_next }
                
                let interface = ptr?.pointee
                let addrFamily = interface?.ifa_addr.pointee.sa_family
                if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                    
                    let name = String(cString: (interface?.ifa_name)!)
                    if name == "en0" {
                        var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                        getnameinfo(interface?.ifa_addr, socklen_t((interface?.ifa_addr.pointee.sa_len)!), &hostname, socklen_t(hostname.count), nil, socklen_t(0), NI_NUMERICHOST)
                        address = String(cString: hostname)
                    }
                }
            }
            freeifaddrs(ifaddr)
        }
        return address
    }
}

extension Bundle {
    
    func certificate(named: String) -> SecCertificate {
        
        let cerURL = self.url(forResource: named, withExtension: "cer")!
        let cerData = try! Data(contentsOf: cerURL)
        return SecCertificateCreateWithData(nil, cerData as NSData)!
    }
    
    func identity(named: String, password: String) -> SecIdentity {
        
        let p12URL = Bundle.main.url(forResource: named, withExtension: "p12")!
        let p12Data = try! Data(contentsOf: p12URL)
        var importResult: CFArray? = nil
        let err = SecPKCS12Import(
            p12Data as NSData,
            [kSecImportExportPassphrase: password] as NSDictionary,
            &importResult)
        assert(err == errSecSuccess)
        let identityDicts = importResult! as! [[String:Any]]
        
        return identityDicts.first![kSecImportItemIdentity as String]! as! SecIdentity
    }
}

