//
//  ViewController.swift
//  AlmamofireTestProject
//
//  Created by Menjie Mao on 2019-09-26.
//  Copyright © 2019 Menjie Mao. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        BankIDManager.sharedInstance.authenticate{ result in
            switch result{
            case .success(let status):
                print("Response Status: \(status)")
            case .failure(let error):
                print(error)
                break
            }
        }
    }
    
    


}

